# jupyter instance

This is my ipython notebook sandbox/playground. There are a collection of python fiddlebits, test fragments, and some snippets for me to reference or practice. 

Feel free to browse around and be impressed, or point and laugh. But laugh only if you've done better, then made it public for all to see. And in that case, I might like to check out your work.